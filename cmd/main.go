package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/spda-project/authservice/internal/adapter/jwt"
	"gitlab.com/spda-project/authservice/internal/adapter/postgres"
	"gitlab.com/spda-project/authservice/internal/controller"
	"gitlab.com/spda-project/authservice/internal/service/auth"
	"gitlab.com/spda-project/authservice/pkg/logger"
)

func main() {
	configPath := flag.String("config-file", "./config.yaml", "Path to config file")
	flag.Parse()

	ctx := context.Background()
	log := logger.New()

	cfg, err := parseConfig(*configPath)
	if err != nil {
		log.Error("can't parse config file: %v", err)

		os.Exit(1)
	}

	server, err := initServer(cfg.Server, &cfg.Database, &cfg.JWT)
	if err != nil {
		log.Error("can't init server: %v", err)

		os.Exit(1)
	}

	log.Info("server initialized")

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	go func() {
		log.Error(server.ListenAndServe().Error())

		os.Exit(1)
	}()
	log.Info(fmt.Sprintf("server is ready to listen and serve on %s", server.Addr))

	killSignal := <-interrupt
	switch killSignal {
	case os.Interrupt:
		log.Warning("got SIGINT...")
	case syscall.SIGTERM:
		log.Warning("got SIGTERM...")
	}

	log.Info("server is shutting down...")
	err = server.Shutdown(ctx)
	if err != nil {
		log.Error("server can't shutdown: %v", err)

		os.Exit(1)
	}

	log.Info("done!")
}

func initServer(serverCfg controller.Config, dbCfg *postgres.Config, jwtCfg *jwt.Config) (*http.Server, error) {
	storage, err := postgres.NewDB(dbCfg)
	if err != nil {
		return nil, fmt.Errorf("can't create database: %w", err)
	}

	tokenizer, err := jwt.NewTokenizer(jwtCfg)
	if err != nil {
		return nil, fmt.Errorf("can't create tokenizer: %w", err)
	}

	authService := auth.NewService(storage, tokenizer)

	return controller.NewServer(&serverCfg, authService)
}
