package main

import (
	"fmt"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v3"

	"gitlab.com/spda-project/authservice/internal/adapter/jwt"
	"gitlab.com/spda-project/authservice/internal/adapter/postgres"
	"gitlab.com/spda-project/authservice/internal/controller"
)

type Config struct {
	Server   controller.Config `yaml:"server"`
	Database postgres.Config   `yaml:"database"`
	JWT      jwt.Config        `yaml:"jwt"`
}

func parseConfig(configPath string) (*Config, error) {
	filename, err := filepath.Abs(configPath)
	if err != nil {
		return nil, fmt.Errorf("can't parse path to config: %w", err)
	}

	file, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("can't read config file: %w", err)
	}

	var cfg Config

	err = yaml.Unmarshal(file, &cfg)
	if err != nil {
		return nil, fmt.Errorf("can't unmarshal config: %w", err)
	}

	return &cfg, nil
}
