FROM golang:alpine3.18 as builder

RUN mkdir /app
WORKDIR /app
COPY go.* ./
RUN go mod download
COPY . ./
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -o application ./cmd

FROM alpine:3.18.4
COPY --from=builder /app/application /app/application
COPY --from=builder /app/cmd/config.yml /app/config.yml
CMD ["/app/application", "-config-path", "/app/config.yml"]
