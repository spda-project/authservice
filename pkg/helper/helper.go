package helper

func GetPointer[T any](v T) *T {
	return &v
}

func GetValueFromPointer[T any](v *T) T {
	var val T

	if v == nil {
		return val
	}

	return *v
}
