package logger

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"
)

const ctxLoggerKey = "ctxLoggerKey"

type Logger struct {
	logger *log.Logger
}

func New() *Logger {
	l := log.New(os.Stdout, "", log.LUTC)
	return &Logger{
		logger: l,
	}
}

func (l *Logger) WithContext(ctx context.Context) context.Context {
	return context.WithValue(ctx, ctxLoggerKey, l)
}

func GetLogger(ctx context.Context) *Logger {
	return ctx.Value(ctxLoggerKey).(*Logger)
}

func (l *Logger) Info(msg string, args ...any) {
	l.logger.Printf("INFO: "+msg, args...)
}

func (l *Logger) Warning(msg string, args ...any) {
	l.logger.Printf("WARNING: "+msg, args...)
}

func (l *Logger) Error(msg string, args ...any) {
	l.logger.Printf("ERROR: "+msg, args...)
}

func (l *Logger) WithValues(values map[string]interface{}) *Logger {
	newLogger := New()

	prefix := strings.Builder{}
	for key := range values {
		val := fmt.Sprintf("%v", values[key])
		prefix.WriteString(key + "=" + val + ", ")
	}

	oldPrefix := l.logger.Prefix()
	newLogger.logger.SetPrefix(oldPrefix + ", " + prefix.String())

	return newLogger
}

func (l *Logger) WithError(err error) *Logger {
	newLogger := New()

	oldPrefix := l.logger.Prefix()
	newLogger.logger.SetPrefix(oldPrefix + ", " + "error=" + err.Error())

	return newLogger
}
