package postgres

import (
	"errors"
	"fmt"
	"os"
)

type Config struct {
	Host        string `yaml:"host"`
	Port        uint16 `yaml:"port"`
	UsernameEnv string `yaml:"username_env"`
	PasswordEnv string `yaml:"password_env"`
	DatabaseEnv string `yaml:"database_env"`
	SSLEnabled  bool   `yaml:"ssl"`
}

func generateConnectionString(cfg *Config) (string, error) {
	var sslmode string

	if !cfg.SSLEnabled {
		sslmode = "disable"
	} else {
		return "", errors.New("unsupported parameter: ssl=true")
	}

	username := os.Getenv(cfg.UsernameEnv)
	if username == "" {
		return "", errors.New("empty env: username")
	}

	password := os.Getenv(cfg.PasswordEnv)
	if password == "" {
		return "", errors.New("empty env: password")
	}

	dbname := os.Getenv(cfg.DatabaseEnv)
	if dbname == "" {
		return "", errors.New("empty env: database")
	}

	return fmt.Sprintf(
		"user=%s password=%s dbname=%s host=%s port=%d sslmode=%s",
		username, password, dbname, cfg.Host, cfg.Port, sslmode,
	), nil
}
