package postgres

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type DB struct {
	DBConn *sqlx.DB
}

func NewDB(cfg *Config) (*DB, error) {
	conn, err := generateConnectionString(cfg)
	if err != nil {
		return nil, fmt.Errorf("can't generate connection string: %w", err)
	}

	db, err := sqlx.Connect("postgres", conn)
	if err != nil {
		return nil, fmt.Errorf("can't connect to database: %w", err)
	}

	err = db.Ping()
	if err != nil {
		return nil, fmt.Errorf("can't ping database: %w", err)
	}

	return &DB{
		DBConn: db,
	}, nil
}
