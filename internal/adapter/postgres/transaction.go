package postgres

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

func (db *DB) executeTransaction(ctx context.Context, operations func(tx *sqlx.Tx) error) error {
	tx, errBegin := db.DBConn.BeginTxx(ctx, nil)
	if errBegin != nil {
		return fmt.Errorf("can't begin the transaction: %w", errBegin)
	}

	errExec := operations(tx)
	if errExec != nil {
		err := fmt.Errorf("can't execute the transaction: %w", errExec)

		errRollback := tx.Rollback()
		if errRollback != nil {
			return fmt.Errorf("can't rollback the transaction: %v, errExec: %w", errRollback, err)
		}

		return err
	}

	errCommit := tx.Commit()
	if errCommit != nil {
		err := fmt.Errorf("can't commit the transaction: %w", errCommit)

		errRollback := tx.Rollback()
		if errRollback != nil {
			return fmt.Errorf("can't rollback the transaction: %v, errExec: %w", errRollback, err)
		}

		return err
	}

	return nil
}
