package postgres

import (
	"time"

	"github.com/google/uuid"

	"gitlab.com/spda-project/authservice/internal/service/auth"
	"gitlab.com/spda-project/authservice/pkg/helper"
)

type User struct {
	Id        uuid.UUID `db:"id"`
	Name      string    `db:"name"`
	Username  string    `db:"username"`
	Email     *string   `db:"email"`
	IsAdmin   bool      `db:"is_admin"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}

func convertUserToService(user *User) *auth.User {
	if user == nil {
		return nil
	}

	return &auth.User{
		Name:     user.Name,
		Username: user.Username,
		Email:    helper.GetValueFromPointer(user.Email),
	}
}

type Credentials struct {
	UserID    uuid.UUID `db:"user_id"`
	Password  string    `db:"password"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}

func convertCredsToService(cred *Credentials) *auth.Credentials {
	if cred == nil {
		return nil
	}

	return &auth.Credentials{
		Password: cred.Password,
	}
}
