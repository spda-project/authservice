package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"

	"gitlab.com/spda-project/authservice/internal/service/auth"
)

const getCredsByUsernameQuery = `
SELECT c.user_id, c.password, c.created_at, c.updated_at
FROM creds c
INNER JOIN users u on u.id = c.user_id
WHERE u.username = $1
`

func (db *DB) GetCredentialsByUsername(ctx context.Context, username string) (*auth.Credentials, error) {
	var creds Credentials

	if err := db.DBConn.GetContext(ctx, &creds, getCredsByUsernameQuery, username); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, auth.NewUserNotFoundError(username)
		}

		return nil, fmt.Errorf("can't exec get-creds-by-username-query: %w", err)
	}

	return convertCredsToService(&creds), nil
}

const getUserByUsernameQuery = `
SELECT id, name, username, email, created_at, updated_at
FROM users
WHERE username = $1
`

func (db *DB) GetUserByUsername(ctx context.Context, username string) (*auth.User, error) {
	var user User

	if err := db.DBConn.GetContext(ctx, &user, getUserByUsernameQuery, username); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, auth.NewUserNotFoundError(username)
		}

		return nil, fmt.Errorf("can't exec get-user-by-username-query: %w", err)
	}

	return convertUserToService(&user), nil
}

const userExistByUsernameQuery = `
SELECT exists(
    SELECT 1
    FROM users
	WHERE username = $1
)`

func (db *DB) CheckUserExistByUsername(ctx context.Context, username string) (bool, error) {
	var exists bool

	if err := db.DBConn.GetContext(ctx, &exists, userExistByUsernameQuery, username); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, nil
		}

		return false, fmt.Errorf("can't exec user-exist-by-username query: %w", err)
	}

	return exists, nil
}

const createUserQuery = `
INSERT INTO users(id, name, username, email)
VALUES (:id, :name, :username, :email)
`

const createCredsQuery = `
INSERT INTO creds(user_id, password)
VALUES (:user_id, :password)
`

func (db *DB) CreateUser(ctx context.Context, user *auth.User, creds *auth.Credentials) error {
	currentTime := time.Now()

	usr := User{
		Id:        uuid.New(),
		Name:      user.Name,
		Username:  user.Username,
		Email:     &user.Email,
		CreatedAt: currentTime,
		UpdatedAt: currentTime,
	}

	cred := Credentials{
		UserID:    usr.Id,
		Password:  creds.Password,
		CreatedAt: currentTime,
		UpdatedAt: currentTime,
	}

	if err := db.executeTransaction(ctx, func(tx *sqlx.Tx) error {
		if _, err := tx.NamedExecContext(ctx, createUserQuery, usr); err != nil {
			return fmt.Errorf("can't exec create-user-query: %w", err)
		}

		if _, err := tx.NamedExecContext(ctx, createCredsQuery, cred); err != nil {
			return fmt.Errorf("can't exec create-creds-query: %w", err)
		}

		return nil
	}); err != nil {
		return fmt.Errorf("can't create user: %w", err)
	}

	return nil
}

const checkIsUserAdminQuery = `
SELECT is_admin
FROM users
WHERE username = $1
`

func (db *DB) CheckIsUserAdmin(ctx context.Context, username string) (bool, error) {
	var isAdmin bool

	if err := db.DBConn.GetContext(ctx, &isAdmin, checkIsUserAdminQuery, username); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, auth.NewUserNotFoundError(username)
		}

		return false, fmt.Errorf("can't exec check-is-user-admin-query: %w", err)
	}

	return isAdmin, nil
}
