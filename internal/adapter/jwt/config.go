package jwt

import "time"

type Config struct {
	PrivateKeyEnv              string        `yaml:"private_key_env"`
	AccessTokenExpirationTime  time.Duration `yaml:"access_token_expiration_time"`
	RefreshTokenExpirationTime time.Duration `yaml:"refresh_token_expiration_time"`
}
