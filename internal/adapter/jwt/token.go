package jwt

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"

	"gitlab.com/spda-project/authservice/internal/service/auth"
)

const issuer = "spda-auth-service"

func (s *Tokenizer) GenerateToken(_ context.Context, user *auth.User) (*auth.Token, error) {
	userData := convertUserToUserData(user)

	now := float64(time.Now().Unix())

	accessToken, err := s.createTokenStr(userData, now+s.AccessTokenExpirationTime.Seconds())
	if err != nil {
		return nil, fmt.Errorf("can't create access token: %w", err)
	}

	refreshToken, err := s.createTokenStr(userData, now+s.RefreshTokenExpirationTime.Seconds())
	if err != nil {
		return nil, fmt.Errorf("can't create refresh token: %w", err)
	}

	return &auth.Token{
		Access:  accessToken,
		Refresh: refreshToken,
	}, nil
}

func (s *Tokenizer) CheckToken(_ context.Context, token string) error {
	_, err := jwt.Parse(token, s.verify)
	if err != nil {
		switch {
		case errors.Is(err, jwt.ErrTokenExpired) || errors.Is(err, jwt.ErrTokenNotValidYet):
			return auth.NewTokenExpiredError()
		default:
			return fmt.Errorf("can't parse token: %w", err)
		}
	}

	return nil
}

func (s *Tokenizer) GetUsername(_ context.Context, token string) (string, error) {
	t, err := jwt.Parse(token, s.verify)
	if err != nil {
		switch {
		case errors.Is(err, jwt.ErrTokenExpired) || errors.Is(err, jwt.ErrTokenNotValidYet):
			return "", auth.NewTokenExpiredError()
		default:
			return "", fmt.Errorf("can't parse token: %w", err)
		}
	}

	return t.Claims.GetSubject()
}

func (s *Tokenizer) createTokenStr(userData *UserData, expTime float64) (string, error) {
	token := jwt.NewWithClaims(s.signingMethod, jwt.MapClaims{
		"exp": expTime,
		"iss": issuer,
		"sub": userData.Username,
	})

	return token.SignedString([]byte(s.jwtKey))
}

func (s *Tokenizer) verify(token *jwt.Token) (interface{}, error) {
	if token.Method != s.signingMethod {
		return nil, errors.New("invalid signing method")
	}

	return []byte(s.jwtKey), nil
}
