package jwt

import (
	"errors"
	"os"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

type Tokenizer struct {
	jwtKey                     string
	signingMethod              jwt.SigningMethod
	AccessTokenExpirationTime  time.Duration
	RefreshTokenExpirationTime time.Duration
}

func NewTokenizer(cfg *Config) (*Tokenizer, error) {
	privateKey := os.Getenv(cfg.PrivateKeyEnv)
	if privateKey == "" {
		return nil, errors.New("empty env: private key")
	}

	return &Tokenizer{
		jwtKey:                     privateKey,
		signingMethod:              jwt.SigningMethodHS512,
		AccessTokenExpirationTime:  cfg.AccessTokenExpirationTime,
		RefreshTokenExpirationTime: cfg.RefreshTokenExpirationTime,
	}, nil
}
