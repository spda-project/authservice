package jwt

import "gitlab.com/spda-project/authservice/internal/service/auth"

type UserData struct {
	Username string
}

func convertUserToUserData(user *auth.User) *UserData {
	if user == nil {
		return &UserData{}
	}

	return &UserData{
		Username: user.Username,
	}
}
