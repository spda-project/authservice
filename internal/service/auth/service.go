package auth

import (
	"context"
)

type UserStorage interface {
	GetCredentialsByUsername(ctx context.Context, username string) (*Credentials, error)
	GetUserByUsername(ctx context.Context, username string) (*User, error)
	CheckUserExistByUsername(ctx context.Context, username string) (bool, error)
	CreateUser(ctx context.Context, user *User, creds *Credentials) error
	CheckIsUserAdmin(ctx context.Context, username string) (bool, error)
}

type Tokenizer interface {
	GenerateToken(ctx context.Context, user *User) (*Token, error)
	CheckToken(ctx context.Context, token string) error
	GetUsername(ctx context.Context, token string) (string, error)
}

type Service struct {
	userStorage UserStorage
	tokenizer   Tokenizer
}

func NewService(storage UserStorage, tokenizer Tokenizer) *Service {
	return &Service{
		userStorage: storage,
		tokenizer:   tokenizer,
	}
}
