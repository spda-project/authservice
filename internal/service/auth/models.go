package auth

type Token struct {
	Access  string
	Refresh string
}

type Credentials struct {
	Password string
}

type User struct {
	Name     string
	Username string
	Email    string
	UserType *UserType
}

type UserType string

const (
	StudentType UserType = "student"
	HRType      UserType = "hr"
)
