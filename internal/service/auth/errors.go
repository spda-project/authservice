package auth

import "fmt"

type IncorrectCredentialsError struct {
	err error
}

func NewIncorrectCredentialsError(err error) IncorrectCredentialsError {
	return IncorrectCredentialsError{
		err: err,
	}
}

func (e IncorrectCredentialsError) Error() string {
	return fmt.Sprintf("incorrect credentials: %s", e.err.Error())
}

type UserNotFoundError struct {
	username string
}

func NewUserNotFoundError(username string) UserNotFoundError {
	return UserNotFoundError{
		username: username,
	}
}

func (e UserNotFoundError) Error() string {
	return fmt.Sprintf("can't find user by username '%s'", e.username)
}

type UserAlreadyExistError struct {
	username string
}

func NewUserAlreadyExistError(username string) UserAlreadyExistError {
	return UserAlreadyExistError{
		username: username,
	}
}

func (e UserAlreadyExistError) Error() string {
	return fmt.Sprintf("user with username '%s' already exist", e.username)
}

type TokenExpiredError struct{}

func NewTokenExpiredError() TokenExpiredError {
	return TokenExpiredError{}
}

func (e TokenExpiredError) Error() string {
	return "token is expired"
}

type NoAccessError struct {
	username string
}

func NewNoAccessError(username string) NoAccessError {
	return NoAccessError{
		username: username,
	}
}

func (e NoAccessError) Error() string {
	return fmt.Sprintf("user doesn't have access to user '%s'", e.username)
}
