package auth

import (
	"context"
	"fmt"
)

func (s *Service) Register(ctx context.Context, userData *User, password string) error {
	exist, err := s.userStorage.CheckUserExistByUsername(ctx, userData.Username)
	if err != nil {
		return fmt.Errorf("can't check if user exist: %w", err)
	}

	if exist {
		return NewUserAlreadyExistError(userData.Username)
	}

	creds, err := s.generateSecurePassword(password)
	if err != nil {
		return fmt.Errorf("can't generate secure password: %w", err)
	}

	err = s.userStorage.CreateUser(ctx, userData, creds)
	if err != nil {
		return fmt.Errorf("can't create user in storage: %w", err)
	}

	return nil
}
