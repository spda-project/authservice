package auth

func (s *Service) checkPassword(enteredPassword string, creds *Credentials) (bool, error) {
	// todo: add salt and hash entered password, then compare it with pass from creds
	if creds.Password == enteredPassword {
		return true, nil
	}

	return false, nil
}

func (s *Service) generateSecurePassword(password string) (*Credentials, error) {
	// todo: generate random salt and hash password, return salt and generated password
	return &Credentials{
		Password: password,
	}, nil
}
