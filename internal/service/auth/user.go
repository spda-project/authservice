package auth

import (
	"context"
	"fmt"
)

func (s *Service) GetUser(ctx context.Context, accessToken string, usernameToGet string) (*User, error) {
	err := s.tokenizer.CheckToken(ctx, accessToken)
	if err != nil {
		return nil, fmt.Errorf("token is incorrect: %w", err)
	}

	username, err := s.tokenizer.GetUsername(ctx, accessToken)
	if err != nil {
		return nil, fmt.Errorf("can't get username from token: %w", err)
	}

	isAdmin, err := s.userStorage.CheckIsUserAdmin(ctx, username)
	if err != nil {
		return nil, fmt.Errorf("can't check if user is admin: %w", err)
	}

	if isAdmin || username == usernameToGet {
		user, err := s.userStorage.GetUserByUsername(ctx, usernameToGet)
		if err != nil {
			return nil, fmt.Errorf("can't get user from storage: %w", err)
		}

		return user, nil
	}

	return nil, NewNoAccessError(usernameToGet)
}
