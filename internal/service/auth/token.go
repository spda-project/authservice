package auth

import (
	"context"
	"fmt"
)

func (s *Service) RefreshToken(ctx context.Context, token *Token) (*Token, error) {
	err := s.tokenizer.CheckToken(ctx, token.Refresh)
	if err != nil {
		return nil, fmt.Errorf("can't check refresh token: %w", err)
	}

	username, err := s.tokenizer.GetUsername(ctx, token.Refresh)
	if err != nil {
		return nil, fmt.Errorf("can't get username from token: %w", err)
	}

	user, err := s.userStorage.GetUserByUsername(ctx, username)
	if err != nil {
		return nil, fmt.Errorf("can't get user from storage: %w", err)
	}

	t, err := s.tokenizer.GenerateToken(ctx, user)
	if err != nil {
		return nil, fmt.Errorf("can't generate new token: %w", err)
	}

	return t, nil
}
