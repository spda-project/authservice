package auth

import (
	"context"
	"fmt"
)

func (s *Service) Login(ctx context.Context, username, password string) (*Token, error) {
	exist, err := s.userStorage.CheckUserExistByUsername(ctx, username)
	if err != nil {
		return nil, fmt.Errorf("can't check if user exist: %w", err)
	}

	if !exist {
		return nil, NewUserNotFoundError(username)
	}

	creds, err := s.userStorage.GetCredentialsByUsername(ctx, username)
	if err != nil {
		return nil, fmt.Errorf("can't check user credentials: %w", err)
	}

	ok, err := s.checkPassword(password, creds)
	if err != nil {
		return nil, fmt.Errorf("can't check password: %w", err)
	}

	if !ok {
		return nil, NewIncorrectCredentialsError(err)
	}

	user, err := s.userStorage.GetUserByUsername(ctx, username)
	if err != nil {
		return nil, fmt.Errorf("can't get user by username: %w", err)
	}

	t, err := s.tokenizer.GenerateToken(ctx, user)
	if err != nil {
		return nil, fmt.Errorf("can't generate jwt: %w", err)
	}

	return t, nil
}
