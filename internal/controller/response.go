package controller

import (
	"encoding/json"
	"net/http"
)

func Response(w http.ResponseWriter, statusCode int, body interface{}) {
	w.WriteHeader(statusCode)
	w.Header().Set("Content-Type", "application/json")
	if body != nil {
		_ = json.NewEncoder(w).Encode(body)
	}
}
