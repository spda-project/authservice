package controller

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/oapi-codegen/runtime/types"

	"gitlab.com/spda-project/authservice/api"
	"gitlab.com/spda-project/authservice/internal/service/auth"
	"gitlab.com/spda-project/authservice/pkg/helper"
	"gitlab.com/spda-project/authservice/pkg/logger"
)

func convertUserTypeToService(t *api.UserType) *auth.UserType {
	if t == nil {
		return nil
	}

	switch *t {
	case api.Student:
		return helper.GetPointer(auth.StudentType)
	case api.Hr:
		return helper.GetPointer(auth.HRType)
	default:
		return nil
	}
}

func convertUserAPIToUser(user *api.UserDataToRegister) *auth.User {
	return &auth.User{
		Name:     user.Name,
		Email:    string(helper.GetValueFromPointer(user.Email)),
		UserType: convertUserTypeToService(user.UserType),
		Username: user.Username,
	}
}

func convertUserTypeToAPI(t *auth.UserType) api.UserType {
	if t == nil {
		return api.Student
	}

	switch *t {
	case auth.StudentType:
		return api.Student
	case auth.HRType:
		return api.Hr
	default:
		return api.Student
	}
}

func (s *Server) Login(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	log := logger.GetLogger(ctx)

	user := &api.UserCredentials{}
	if err := json.NewDecoder(r.Body).Decode(user); err != nil {
		log.WithError(err).Error("can't decode user data")
		Response(w, http.StatusBadRequest, &api.Error{
			Description: "can't decode user data",
			Message:     "user body is incorrect",
		})

		return
	}

	token, err := s.AuthService.Login(ctx, user.Username, user.Password)
	if err != nil {
		switch {
		case errors.As(err, &auth.UserNotFoundError{}):
			log.WithError(err).Warning("user not found")
			Response(w, http.StatusBadRequest, &api.Error{
				Description: "user provided incorrect username or password",
				Message:     "incorrect credentials during login",
			})
		case errors.As(err, &auth.IncorrectCredentialsError{}):
			log.WithError(err).Warning("user provided incorrect username or password")
			Response(w, http.StatusBadRequest, &api.Error{
				Description: "user provided incorrect username or password",
				Message:     "incorrect credentials during login",
			})
		default:
			log.WithError(err).Error("user can't login")
			Response(w, http.StatusInternalServerError, &api.Error{
				Description: "user can't login",
				Message:     "user can't login",
			})
		}

		return
	}

	Response(w, http.StatusOK, api.Tokens{
		AccessToken:  &token.Access,
		RefreshToken: &token.Refresh,
	})
}

func (s *Server) Register(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	log := logger.GetLogger(ctx)

	user := &api.UserDataToRegister{}
	if err := json.NewDecoder(r.Body).Decode(user); err != nil {
		log.WithError(err).Error("can't decode user data")
		Response(w, http.StatusBadRequest, &api.Error{
			Description: "can't decode user data",
			Message:     "user body is incorrect",
		})

		return
	}

	err := s.AuthService.Register(ctx, convertUserAPIToUser(user), user.Password)
	if err != nil {
		switch {
		case errors.As(err, &auth.UserAlreadyExistError{}):
			log.WithError(err).Warning("user already exist")
			Response(w, http.StatusConflict, &api.Error{
				Description: "user already exist",
				Message:     "username is already taken",
			})
		default:
			log.WithError(err).Error("can't register new user")
			Response(w, http.StatusInternalServerError, &api.Error{
				Description: "can't register new user",
				Message:     "can't register new user",
			})
		}

		return
	}

	Response(w, http.StatusNoContent, nil)
}

func (s *Server) RefreshToken(w http.ResponseWriter, r *http.Request, params api.RefreshTokenParams) {
	ctx := r.Context()
	log := logger.GetLogger(ctx)

	token, err := s.AuthService.RefreshToken(ctx, &auth.Token{
		Refresh: params.RefreshToken,
	})
	if err != nil {
		switch {
		case errors.As(err, &auth.TokenExpiredError{}):
			log.WithError(err).Warning("refresh token is expired")
			Response(w, http.StatusBadRequest, &api.Error{
				Description: "refresh token is expired",
				Message:     "refresh token is expired",
			})
		default:
			log.WithError(err).Error("can't refresh the token")
			Response(w, http.StatusInternalServerError, &api.Error{
				Description: "can't refresh the token",
				Message:     "can't refresh the token",
			})
		}

		return
	}

	Response(w, http.StatusOK, api.Tokens{
		AccessToken:  &token.Access,
		RefreshToken: &token.Refresh,
	})
}

func (s *Server) GetUser(w http.ResponseWriter, r *http.Request, username api.UsernamePath, params api.GetUserParams) {
	ctx := r.Context()
	log := logger.GetLogger(ctx)

	user, err := s.AuthService.GetUser(ctx, params.AccessToken, username)
	if err != nil {
		switch {
		case errors.As(err, &auth.NoAccessError{}):
			log.WithError(err).Warning("user has no access")
			Response(w, http.StatusForbidden, &api.Error{
				Description: "user has no access to get this user",
				Message:     "user has no access",
			})
		case errors.As(err, &auth.TokenExpiredError{}):
			log.WithError(err).Warning("token is expired")
			Response(w, http.StatusBadRequest, &api.Error{
				Description: "token is expired",
				Message:     "access token is expired",
			})
		case errors.As(err, &auth.UserNotFoundError{}):
			log.WithError(err).Warning("user not found")
			Response(w, http.StatusNotFound, &api.Error{
				Description: "user not found",
				Message:     "user with such username not found",
			})
		default:
			log.WithError(err).Error("can't get user")
			Response(w, http.StatusInternalServerError, &api.Error{
				Description: "can't get user",
				Message:     "can't get user",
			})
		}

		return
	}

	Response(w, http.StatusOK, api.UserData{
		Name:     user.Name,
		Email:    types.Email(user.Email),
		UserType: convertUserTypeToAPI(user.UserType),
		Username: user.Username,
	})
}
