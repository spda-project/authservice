package controller

import (
	"fmt"
	"net"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	middleware "github.com/oapi-codegen/nethttp-middleware"

	"gitlab.com/spda-project/authservice/api"
	"gitlab.com/spda-project/authservice/internal/service/auth"
	"gitlab.com/spda-project/authservice/pkg/logger"
)

type Server struct {
	AuthService *auth.Service
}

func NewServer(cfg *Config, authService *auth.Service) (*http.Server, error) {
	s := &Server{
		AuthService: authService,
	}

	router, err := initRouter()
	if err != nil {
		return nil, fmt.Errorf("can't init router: %w", err)
	}

	api.HandlerFromMux(s, router)

	server := &http.Server{
		Handler: router,
		Addr:    net.JoinHostPort("0.0.0.0", strconv.Itoa(cfg.Port)),
	}

	return server, nil
}

func initRouter() (*chi.Mux, error) {
	swagger, err := api.GetSwagger()
	if err != nil {
		return nil, fmt.Errorf("can't load swagger spec: %w", err)
	}

	// Clear out the servers array in the swagger spec, that skips validating
	// that server names match. We don't know how this thing will be run.
	swagger.Servers = nil

	r := chi.NewRouter()

	// Use our validation middleware to check all requests against the OpenAPI schema.
	r.Use(SetHeaders)
	r.Use(middleware.OapiRequestValidator(swagger))
	r.Use(SetLogger)
	r.Use(AccessLog)

	return r, nil
}

func SetHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if origin := r.Header.Get("Origin"); origin != "" {
			w.Header().Set("Access-Control-Allow-Origin", origin)
			w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
			w.Header().Set("Access-Control-Allow-Headers",
				"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		}
		if r.Method == "OPTIONS" {
			return
		}

		next.ServeHTTP(w, r)
	})
}

func SetLogger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log := logger.New().WithValues(map[string]interface{}{
			"method": r.Method,
			"path":   r.URL.Path,
		})
		ctx := log.WithContext(r.Context())

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func AccessLog(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log := logger.GetLogger(r.Context())
		log.Info("Access log")

		next.ServeHTTP(w, r)
	})
}
