# Auth service

Used for user authentication. User can log in, register, refresh token,
and get user information.

## Database migration

To use migration, follow the instructions of [goose](https://github.com/pressly/goose) module.

Install **goose**:

```shell
go install github.com/pressly/goose/v3/cmd/goose@latest
```

### Create migration

Go to *migrations* folder and execute the following command:

```shell
goose postgres "user=postgres password=postgres dbname=auth host=127.0.0.1 port=5432 sslmode=disable" create 'migration name' sql
```

It should create sql file `<current-data>_migration_name.sql`, e.g. `20231109182939_users_and_creds.sql`
